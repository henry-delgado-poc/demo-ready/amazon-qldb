﻿# Amazon QLDB using .NET SDK
REST API to implement proof of concept for Amazon QLDB

# Amazon QLDB
Amazon Quantum Ledger Database (Amazon QLDB) is a fully managed ledger database that provides a transparent, immutable, and cryptographically verifiable transaction log owned by a central trusted authority.

![Amazon QLDB](./api/Documentation/Images/Amazon-QLDB.png)

![Amazon QLDB](./api/Documentation/Images/Amazon-QLDB-2.PNG)

## AWS Requisites
1. Get AWS Credentials for programmatic access to Amazon QLDB
2. Ensure you have AWS Toolkit for Visual Studio
3. Using Visual Studio kit for Visual Studio, create new profile and pass the Access Key ID and Secret Access Key, and account number from previous step 1.

## Proof of Concept
* [Ledger API](./api/Controllers/LedgerController.cs)
* [QLDB Query Examples](./api/Documentation/QLDB-Queries)

## AWS Limitations
* Only 5 Ledgers per region and 40 tital tables -> No possible on enterprise solution

## Links
* [Amazon Quantum Ledger Database](https://docs.aws.amazon.com/qldb/?id=docs_gateway)
* [See Short Amazon Overview Video](https://www.youtube.com/watch?v=jcZ_rsLJrqk&feature=emb_logo) 
* [Amazon QLDB Queries Video](https://www.youtube.com/watch?v=XGeCNr8eOiA)
* [Pricing](https://aws.amazon.com/qldb/pricing/) | [Quotas and Limits](https://docs.aws.amazon.com/qldb/latest/developerguide/limits.html)
* [.NET QLDB Driver Tutorial](https://docs.aws.amazon.com/qldb/latest/developerguide/driver-quickstart-dotnet.html) 
* [Features](https://aws.amazon.com/qldb/features/)