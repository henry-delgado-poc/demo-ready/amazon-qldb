﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Extensions.NETCore.Setup;
using Amazon.IonDotnet.Tree;
using Amazon.IonDotnet.Tree.Impl;
using Amazon.QLDB;
using Amazon.QLDB.Driver;
using Amazon.QLDB.Model;
using Amazon.QLDBSession;
using Microsoft.Extensions.Configuration;

namespace Api.AmazonQldb
{
    /// <summary>
    /// Functionality related to managing AWS Ledger for QLDB
    /// </summary>
    public class LedgerHelper
    {
        private readonly AWSOptions _options;
        private readonly IAmazonQLDB _qldbClient;
        private readonly IValueFactory _valueFactory;


        /// <summary>
        /// Ledger Helper
        /// </summary>
        public LedgerHelper(IConfiguration configuration)
        {
            _options = configuration.GetAWSOptions();
            _qldbClient = _options.CreateServiceClient<IAmazonQLDB>();
            
            _valueFactory = new ValueFactory();
        }

        /// <summary>
        /// Get list of available ledgers
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetLedgers()
        {
            var response = await _qldbClient.ListLedgersAsync(new ListLedgersRequest());
            return response.Ledgers == null || !response.Ledgers.Any()
                ? new List<string>()
                : response.Ledgers.Select(x => x.Name).ToList();

        }

        /// <summary>
        /// Creating QLDB Ledger
        /// </summary>
        /// <param name="ledgerName">name of new ledger</param>        
        public async Task CreateLedgerAsync(string ledgerName)
        {
            if (string.IsNullOrEmpty(ledgerName)) return;
            if (!await CheckIfLedgerExistsAsync(ledgerName))
            {
                Console.WriteLine($"Creating ledger [{ledgerName}]...");
                await _qldbClient.CreateLedgerAsync(new CreateLedgerRequest
                {
                    DeletionProtection = true,
                    Name = ledgerName,
                    PermissionsMode = PermissionsMode.ALLOW_ALL
                });
            }
            else
            {
                Console.WriteLine($"Ledger [{ledgerName}] already exists.");
            }
            await CheckIfLedgerActiveAsync(ledgerName);
        }

        /// <summary>
        /// Deleting ledger
        /// </summary>
        /// <param name="ledgerName">name</param>
        public async Task DeleteLedgerAsync(string ledgerName)
        {
            if (await CheckIfLedgerExistsAsync(ledgerName))
            {
                Console.WriteLine($"Deleting ledger [{ledgerName}]...");
                await _qldbClient.DeleteLedgerAsync(new DeleteLedgerRequest
                {
                    Name = ledgerName
                });
            }
            else
            {
                throw new ArgumentException("Ledger does not exists. Ledger was not deleted.");
            }
        }

        public bool CheckIfTableFieldValueAlreadyExistsAsync(string ledgerName, string tableName, string fieldName, string fieldValue)
        {
            using (IQldbDriver _qldbDriver = QldbDriver.Builder().WithLedger(ledgerName).Build())
            {
                _qldbDriver.Execute(transactionExecutor =>
                {
                    var ionField = _valueFactory.NewString(fieldValue);
                    var response = transactionExecutor.Execute($"SELECT {fieldName.ToUpper()} FROM {tableName} AS x WHERE x.{fieldName.ToUpper()} = {ionField}");
                    return response.Any(x => x.GetField($"{fieldName.ToUpper()}").StringValue == fieldValue);
                });
                return false;
            }
        }

        /// <summary>
        /// Get tables for given ledger
        /// </summary>
        /// <param name="ledgerName"></param>
        /// <returns></returns>
        public async Task<List<string>> GetTablesAsync(string ledgerName)
        {
            var amazonSessionConfig = new AmazonQLDBSessionConfig
            {
                RegionEndpoint = _options.Region
            };
                        
            IQldbDriver driver = QldbDriver.Builder()
               .WithQLDBSessionConfig(amazonSessionConfig)
                .WithLedger(ledgerName)
                .Build();

            if (!await CheckIfLedgerExistsAsync(ledgerName))
                throw new ArgumentException($"Ledger [{ledgerName}] does not exist");
           
            return await Task.Run(() =>
            {                
                return driver.ListTableNames().ToList();
            });


        }


        /// <summary>
        /// Creates a QLDB table
        /// </summary>        
        public async Task CreateTableAsync(string ledgerName, string tableName)
        {
            if (!await CheckIfLedgerExistsAsync(ledgerName))
                throw new ArgumentException($"Ledger [{ledgerName}] does not exist");

            using (IQldbDriver _qldbDriver = QldbDriver.Builder().WithLedger(ledgerName).Build())
            {
                if (!await CheckIfTableExistsAsync(_qldbDriver, tableName))
                {
                    Console.WriteLine($"Creating table [{tableName}]...");
                    _qldbDriver.Execute(transactionExecutor =>
                    {
                        transactionExecutor.Execute($"CREATE TABLE {tableName}");
                    });

                }
                else
                {
                    Console.WriteLine($"Table [{tableName}] already exists.");
                }
            }
        }

        /// <summary>
        /// Create table index
        /// </summary>
        /// <param name="ledgerName"></param>
        /// <param name="tableName">table name</param>
        /// <param name="field">table field</param>        
        public async Task CreateIndex(string ledgerName, string tableName, string field)
        {
            if (!await CheckIfLedgerExistsAsync(ledgerName))
                throw new ArgumentException($"Ledger [{ledgerName}] does not exist");

            using (IQldbDriver _qldbDriver = QldbDriver.Builder().WithLedger(ledgerName).Build())
            {
                if (!await CheckIfIndexExistsAsync(_qldbDriver, tableName, field))
                {

                    Console.WriteLine($"Index does not exist. Creating index for table [{tableName}] for field [{field}]");


                    _qldbDriver.Execute(TransactionExecutor =>
                    {
                        TransactionExecutor.Execute($"CREATE INDEX ON {tableName}({field})");
                    });

                }
                else
                {
                    Console.WriteLine($"Index already exists for table [{tableName}] and field [{field}]");
                }
            }
        }

        private async Task<bool> CheckIfIndexExistsAsync(IQldbDriver _qldbDriver, string tableName, string field)
        {
            return await Task.Run(() =>
            {
                var result = _qldbDriver.Execute(transactionExecutor =>
                                {
                                    var ionTableName = this._valueFactory.NewString(tableName);
                                    return transactionExecutor.Execute($"SELECT * FROM information_schema.user_tables WHERE name = {ionTableName}");
                                });

                if (result.Any())
                {
                    var indexes = result.First().GetField("indexes") as IIonList;
                    foreach (var idx in indexes)
                    {
                        var expr = idx.GetField("expr").StringValue;
                        if (expr.Contains(field))
                            return true;
                    }
                }

                return false;
            });
        }

        private async Task<bool> CheckIfTableExistsAsync(IQldbDriver _qldbDriver, string tableName)
        {
            Console.WriteLine($"Checking if table [{tableName}] already exists");
            return await Task.Run(() =>
            {
                return _qldbDriver.ListTableNames().FirstOrDefault(x => x.Equals(tableName, StringComparison.InvariantCultureIgnoreCase)) != null;
            });
        }

        /// <summary>
        /// Before creating any tables, our ledger MUST  be active
        /// </summary>        
        private async Task CheckIfLedgerActiveAsync(string ledgerName)
        {
            Console.WriteLine("Waiting for ledger to become active");
            var state = LedgerState.CREATING;
            do
            {
                var response = await _qldbClient.DescribeLedgerAsync(new DescribeLedgerRequest
                {
                    Name = ledgerName
                });
                state = response.State;
                Thread.Sleep(2000);
            } while (state != LedgerState.ACTIVE);
            Console.WriteLine("Ledger is active.");
        }

        private async Task<bool> CheckIfLedgerExistsAsync(string name)
        {
            Console.WriteLine($"Checking if ledger [{name}] exists...");
            var response = await _qldbClient.ListLedgersAsync(new ListLedgersRequest());
            return response.Ledgers.FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase)) != null;
        }
    }
}
