﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Api.AmazonQldb.Controllers
{
    /// <summary>
    /// Not useful API: stopped poc on QLDB due to limitations
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class LedgerController : ControllerBase
    {
        private readonly ILogger<LedgerController> _logger;
        private readonly IConfiguration _configuration;
        private readonly LedgerHelper _helper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        public LedgerController(ILogger<LedgerController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _helper = new LedgerHelper(_configuration);
        }

        /// <summary>
        /// Gets list of available ledgers from Amazon QLDB (for demo only)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<string>>> Get()
        {
            var data = await _helper.GetLedgers();
            return Ok(data);
        }

        /// <summary>
        /// Creates new ledger
        /// </summary>
        /// <param name="ledgerName">name</param>
        [HttpPost]
        public async Task<ActionResult> Post(string ledgerName)
        {
            if (string.IsNullOrEmpty(ledgerName))
                return BadRequest($"Missing {nameof(ledgerName)}");

            await _helper.CreateLedgerAsync(ledgerName);
            return Ok();
        }

        /// <summary>
        /// Deletes ledger
        /// </summary>
        /// <param name="name">ledger name</param>        
        [HttpDelete]
        public async Task<ActionResult> Delete(string name)
        {
            if (string.IsNullOrEmpty(name))
                return BadRequest($"Missing {nameof(name)}");

            try
            {
                await _helper.DeleteLedgerAsync(name);
                return Ok();
            }
            catch (ArgumentException ae)
            {
                _logger.LogDebug(ae, "Attempted to delete ledger name that does not exists");
                return NotFound();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Disable"))
                    return Unauthorized();

                _logger.LogError(ex, "Could not delete ledger");
                return BadRequest(); //for demo purposes
            }
        }
    }
}
