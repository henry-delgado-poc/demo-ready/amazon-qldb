# QLDB Queries | Insert Penetrations
```
INSERT INTO Penetration
<<
{
    'Id': '1',
    'Description': 'Building penetration entry 1',
    'ProjectId': '1',
    'UserId': '1000'    
},
{
    'Id': '2',
    'Description': 'Building penetration entry 2',
    'ProjectId': '1',
    'UserId': '1000'    
},
{
    'Id': '3',
    'Description': 'Building penetration entry 3',
    'ProjectId': '1',
    'UserId': '1000'    
},
{
    'Id': '4',
    'Description': 'Building penetration entry 4',
    'ProjectId': '1',
    'UserId': '1000'    
},
{
    'Id': '5',
    'Description': 'Building penetration entry 5',
    'ProjectId': '1',
    'UserId': '1000'    
}
>>
```